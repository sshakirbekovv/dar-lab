import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Button from '../../components/button/Button';
import styles from './LoginPage.module.scss';
import { Profile } from '../../shared/type';
import { useDispatch, useSelector } from 'react-redux';
import { startLogin } from '../../shared/redux/auth/auth.actions';
import { selectProfile, selectLoginLoading, selectLoginError } from '../../shared/redux/auth/auth.selectors';

const LoginPage: React.FC = () => {

    // const {dispatch} = useContext(AppContext);
    const history = useHistory();
    const [form, setForm] = useState({username: '', password: ''});     
    const dispatch = useDispatch();
    const profile = useSelector(selectProfile)
    const loginLoading = useSelector(selectLoginLoading);
    const loginError = useSelector(selectLoginError);

    const setLocal = (user: Profile) => {
        localStorage.setItem('user', JSON.stringify(user));
    }

    const handleLogin = () => {
        if (form.password && form.username) {
            dispatch(startLogin(form.username, form.password));            
        }
    }

      
    useEffect(() => {
        if (profile) {
            history.replace('/');
        }
    }, [profile])

    return (
        <div className={styles.wrapper}>
            <h2>Log in</h2>
            <div>
                {loginError}
            </div>
            <div className={styles.controls}>
                <input 
                    className={styles.input} 
                    value={form.username} 
                    type="text" 
                    name="username" 
                    onChange={(e) => {setForm(state => ({...state, username: e.target.value}))}}
                    placeholder="Enter your username" />
                <input 
                    className={styles.input} 
                    value={form.password}
                    type="text" 
                    name="password"
                    onChange={(e) => {setForm(state => ({...state, password: e.target.value}))}} 
                    placeholder="Enter your password" />
            </div>
            <Button title="Login" variant="primary" onClick={handleLogin} disabled={ loginLoading }/>
        </div>
    )
}

export default LoginPage;
