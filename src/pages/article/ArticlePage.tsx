import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import CommentForm from '../../components/comment-form/CommentForm';
import { getArticle } from '../../shared/api';
import { fetchArticle, fetchArticles } from '../../shared/redux/articles/articles.actions';
import { selectArticle } from '../../shared/redux/articles/articles.selectors';
import { Article } from '../../shared/type';
import styles from './ArticlePage.module.scss';

const ArticlePage: React.FC = () => {
  
    const {articleId} = useParams<{articleId: string}>();

    const dispatch = useDispatch();
    const article = useSelector(selectArticle);


    useEffect(() => {
        if (articleId) {
          dispatch(fetchArticle({ articleId }));
        }
      }, [articleId, dispatch]);
    
    
   return (
        <div className={styles.ArticlePage}>
            <div className={styles.container}>
                {
                    article && (
                        <div className={styles.ArticlePage__item}>
                            <div className={styles.ArticlePage__item__title}>
                                {article.title}
                            </div>
                            <div className={styles.ArticlePage__item__description}
                            dangerouslySetInnerHTML={{__html: article.description}}
                            >
                            </div>
                            <CommentForm />
                        </div>
                    )
                }
            </div>
       </div>
   )
}

export default ArticlePage;