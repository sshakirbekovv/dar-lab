import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import ArticleMain from '../../components/articleMain/ArticleMain';
import {getArticles } from '../../shared/api';
import { fetchArticles } from '../../shared/redux/articles/articles.actions';
import { selectArticles } from '../../shared/redux/articles/articles.selectors';
import { Article } from '../../shared/type';
import styles from './ArticlePages.module.scss';

const ArticlePages: React.FC = () => {
    const { categoryId } = useParams<{ categoryId: string }>();

    // const [articleItems, setArticleItems] = useState<Article[]>([]);
  
    const dispatch = useDispatch();

    
    const articles = useSelector(selectArticles);
    console.log('Articles: ',articles)

    useEffect(() => { 
      // const fetchData = async () => {
      //   const res = await getArticles(categoryId);
      //   setArticleItems(res.data);
      //  }
      //  fetchData ();
       dispatch(fetchArticles({ categoryId }));
    }, [categoryId, dispatch]);

  return(
    <div className={styles.ArticlePages}>
          <ArticleMain articleItems = {articles} />
    </div>
  );
}

export default ArticlePages;