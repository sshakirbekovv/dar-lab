import React, { useEffect, useState } from 'react'
import styles from './Input.module.scss'



type Props = {
    label: string;
    name: string;
    value: string;
    type: 'text';
    required?: boolean;
    onChange: (diffValue: string) => void;
};
  

const Input: React.FC<Props> = ({label, name, value, type, required}) => {
    
    const [diffValue, setdiffValue] = useState<string>(value);
    const [errorWas, setErrorWas] = useState(false);
    const [error, setError] = useState('');

    const emailHandler = (email: string) => {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(String(email).toLowerCase())){
            setError('Invalid email!')
            if(!email) {
                setError('Email can not be empty!')
             }
        } else {
            setError('')
        }
    }
   

    const usernameHandler = (username: string) => {
        if(username.length <= 4) {
                setError('Username required more than 4 letters!')
            if(!username) {
                setError('Username can not be empty!')
             }
         } 
         else  if(username.length >= 8) {
            setError('Username required less than 8 letters!')
         } 
         else  if(/^\s+$/.test(username)){
            setError('Invalid username!')
         }
         else {
            setError('')
         }
    }
    useEffect(() => {
        if (required) { 
            if (name === 'email'){
                emailHandler(diffValue)
            }
            if (name === 'username'){
                usernameHandler(diffValue)
            }
        }
     }, [diffValue])

   const blurHandle = (e: { target: { name: string; }; }) => {
    if (required) { 
        switch (e.target.name) {
            case 'username':
                setErrorWas(true)
                break
            case 'email':
                setErrorWas(true)
                break
        }
    }
}

    return(
        <div className={styles.Input}>
            <label>{label}</label> <br/>
            {(errorWas && error) && <div className={styles.validError}>{error}</div>}
            <input onBlur={e => blurHandle(e)} value={diffValue} name={name} type={type} onChange={e => setdiffValue(e.target.value)} />
        </div>
    )

}

export default Input;