import React, { useEffect, useState } from 'react';
import AppContext from '../../shared/app.context';
import Button from '../button/Button';
import Input from '../input/Input';
import styles from './CommentForm.module.scss';
import Profile from '../profile/Profile';
import { useSelector } from 'react-redux';
import { selectProfile } from '../../shared/redux/auth/auth.selectors';


const CommentForm: React.FC = () => {

    const [validForm, setValidForm] = useState(false);

    const [fields, setFields] = useState<{
        username: string;
        email: string;
        comment: string;
    }>({
        username: '',
        email: '',
        comment: '',
    });

    

    const onSubmit = () => {
        console.log('Form submitted', fields)
    }
 
    const profile = useSelector(selectProfile);
    useEffect(() => {
        //TODO: validate form fields!
        console.log('FORM CHANGED', fields)
        if(fields.username === '' || fields.email === '') {
            console.log("False")
            setValidForm(false)
        } else {
            console.log("True")
            setValidForm(true)
        }
    }, [fields])

    const fieldChange = (fieldName: string, value: any) => {
        setFields(oldState => ({
            ...oldState,
            [fieldName]: value
        }));
    }

    return (
        <AppContext.Consumer>
        {() => profile ?
        (<div className={styles.wrapper}>
            <div className={styles.user}>
                <div className={styles.formControl}>
                   <Input 
                       label={'Username'}
                       value={fields.username}
                       name={'username'}
                       type={'text'}
                       onChange={(diffValue) => fieldChange('username', diffValue)}
                       required
                    />
                </div>
                <div className={styles.formControl}>
                <Input 
                    label={'Email'}
                    value={fields.email}
                    name={'email'}
                    type={'text'}
                    onChange={value => fieldChange('email', value)}
                    required
                />
                </div>
                <Profile username={profile.username} avatar={profile.avatar} />
            </div>
            <div className={styles.comment}>
                <div className={styles.formControl}>
                    <label>Comment</label>
                    <textarea name="comment" value={fields.comment} onChange={e => fieldChange('comment', e.target.value)} />
                </div>
            </div>
            <div className={styles.controls}>
                <Button title="Отправить" variant="primary" onClick={onSubmit} disabled={!validForm} />
            </div>
        </div> ) : <div className={styles.wrapper}>Чтобы оставить комментарий войдите в аккаунт</div> }
        </AppContext.Consumer>

    )
}

export default CommentForm;
