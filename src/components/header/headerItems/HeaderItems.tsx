import React from 'react';

type Props = {
  items: string[];
}

const HeaderItems: React.FC<Props> = ({items}) => {

  return (
    <>
      {items.map((item, index) => (
          <li key={index}> {item} </li>
        ))}
    </>
  );
};
export default HeaderItems;
