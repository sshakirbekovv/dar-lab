import React, { useContext } from 'react';
import styles from './Header.module.scss';
import logo from '../../assets/images/logo.png'
import Button from '../button/Button';
import HeaderItems from './headerItems/HeaderItems';
// import { Profile } from '../../shared/type';
import { useHistory } from 'react-router-dom';
import AppContext from '../../shared/app.context';
import Profile from '../profile/Profile';
import { useSelector } from 'react-redux';
import { selectCategories } from '../../shared/redux/categories/categories.selectors';
import { selectProfile } from '../../shared/redux/auth/auth.selectors';

// type Props = {
//     items: string[]
// }  

const items = ['О нас', 'Обучение', 'Сообщество', 'Медиа'];

const Header: React.FC = () => {

    const {dispatch} = useContext(AppContext);
    // const {state: {profile}} = useContext(AppContext);

    const profile = useSelector(selectProfile);


    const history = useHistory();

    const goToLogin = () => {
        history.push('/login');
    }



    return (
        <div className={styles.Header}>
            <header>
                <div className={styles.container}>
                   <div className={styles.Header__body}>
                      <div className={styles.Header__logo}>
                         <img src={logo} className={styles.Header__img} alt="logo"/>
                     </div>
                        <div  className={styles.Header__menu}>
                            <ul>
                              <HeaderItems items={items}/>
                            </ul>
                        </div>
                         {
                            profile ? <Profile username={profile.username} avatar={profile.avatar} />:
                                <div className={styles.header_login}>
                                <Button title = {'Login'} onClick={goToLogin} />
                                </div>
                         }
                    </div>
               </div>
            </header>
        </div>
    );
}

export default Header;
