import React from 'react';
import styles from './ArticleList.module.scss'
import {Article} from '../../../shared/type'
import { Link } from 'react-router-dom';
  
type Props = {
    articleItem: Article,
}
  
const ArticleList: React.FC<Props> = ({articleItem}) => {
    const date = new Date(articleItem.created_at);
    return (
        <Link to={`/article/${articleItem.id}`}>
        <div className={styles.ArticleList}>
            <article className={styles.ArticleList__item}>
                <div className={styles.ArticleList__content}>
                    <h3 className={styles.ArticleList__content__title}>{articleItem.title}</h3>
                    <p className={styles.ArticleList__content__annotation}>{articleItem.annotation}</p>
                    <p className={styles.ArticleList__content__date}>{`${date.getDate()} ${date.toLocaleString('ru', { month: 'long' })}`}</p>
                </div>
                <div  className={styles.ArticleList__image}>
                    <img src={`https://dev-darmedia-uploads.s3.eu-west-1.amazonaws.com/${articleItem.image}`} alt="image"/>
                </div>
            </article>
        </div>
        </Link>
    );
}

export default ArticleList;
