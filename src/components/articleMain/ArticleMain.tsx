import React from 'react';
import styles from './ArticleMain.module.scss';
import ArticleList from './articleList/ArticleList';
import {Article} from '../../shared/type';

type Props = {
    articleItems: Article[];
}

const ArticleMain: React.FC<Props> = ({articleItems}) => {

    return (
        <div className={styles.container}>
            <div className={styles.ArticleMain}>
                    <h1>Актуальное</h1>
                    {
                     articleItems && articleItems.map(articleItem => (<ArticleList key={articleItem.id} articleItem = {articleItem} />))
                    }
            </div>
        </div>
    );
}

export default ArticleMain;
