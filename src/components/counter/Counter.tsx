import React, {useEffect, useRef, useState} from 'react';
import styled from 'styled-components';
import CounterButtons from './counterButtons/CounterButtons';
import {CheckLikes} from '../../shared/utils'

const StyledDiv = styled.div`
   background: #800080;
   display: flex;
   justify-content: space-around;
   align-items: center;
   width: 100%;
   padding: 5rem 0;
`
const CountDiv = styled.div`
    font-size: 3rem;
    color: #fff;
`
const MainDiv = styled.div`
     display: flex;
     justify-content: center;
`
const Counter: React.FC = () => {

    const [like, setLike] = useState<number>(0);
    const [submitted, setSubmitted] = useState<boolean>(false);
    const [message,setMessage] = useState<string>("");
    const [error,setError] = useState<string>("");
    const [loading, setLoading] = useState<boolean>(false);
    const initialLoad = useRef<boolean>(false);

    const addLike = () => {
      setLike(like + 1);
    };
  
    const disLike = () => {
      setLike(like ? like - 1 : like);
    };

     useEffect(() => {
         if(!initialLoad.current) {
            initialLoad.current = true;
            return;
         }
         clearMessage();
         setLoading(true);
         const checkAsync = async () => {
            try {
                const res = await CheckLikes(like);
                setMessage(res);
            } catch(err) {
                setError(err);
            }
            setLoading(false);
        }
        checkAsync();
     },[like])

    const clearMessage = () => {
        setMessage("");
        setError("");
    }

    return (
        <MainDiv>
            <StyledDiv>
                <CounterButtons title = {'Дизлайк'} onClick={disLike} disabled={loading}/>
                <CountDiv> 
                    {like} 
                    <div>{`${submitted}`}</div>
                    <CounterButtons onClick={() => setSubmitted(true)} title = {'Submit'} />
                    {message ? <div>{message}</div> : ""}
                    {error ? <div>{error}</div> : ""}
                    {loading && <div>Loading...</div>}
                </CountDiv>
                <CounterButtons title = {'Лайк'} disabled={loading} onClick={addLike} />
            </StyledDiv>
        </MainDiv>
    );
}

export default Counter;