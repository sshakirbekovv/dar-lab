import React from 'react';
import styled from 'styled-components'

type OutlineProps = {
    outlined: boolean;
}

const StyledButtonLikes = styled.button<OutlineProps>`
   color: ${({ outlined }) => (outlined ? '#000' : '#FFFF')};
   background: ${({ outlined }) => (outlined ? 'none' : '#FF0000')};
   border-radius: 5px;
   font-size: 1rem;
   padding: .5rem 1rem;
   border: ${({ outlined }) => (outlined ? 'none' : '#FF0000')};
   cursor: pointer;
   max-height: 2rem;
   outline: none;
   border: none;
   opacity: ${({ disabled }) => (disabled ? '0.5' : 'none')};
`


type Props = {
    title: string;
    outlined?: boolean;
    onClick: () => void;
    disabled?: boolean;
}

const CounterButtons: React.FC<Props> = ({title, outlined, onClick, disabled}) => {
    return (
        <StyledButtonLikes outlined ={!!outlined} onClick={onClick} disabled={disabled}>
            {title}
        </StyledButtonLikes>
    );
}

export default CounterButtons;
