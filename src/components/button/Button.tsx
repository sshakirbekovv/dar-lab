import React from 'react';
import styled from 'styled-components'

type OutlineProps = {
    outlined: boolean;
}

const StyledButton = styled.button<OutlineProps>`
   color: ${({ outlined }) => (outlined ? 'none' : '#FFFF')};
   background: ${({ outlined }) => (outlined ? 'none' : '#9400D3')};
   border-radius: 5px;
   font-size: 1rem;
   padding: .5rem 1rem;
   border: ${({ outlined }) => (outlined ? 'none' : '#9400D3')};
   cursor: pointer;
   opacity: ${({ disabled }) => (disabled ? '0.5' : 'none')};
`


const StyledButtonGhost = styled(StyledButton)`
        color: #000;
        border: 1px solid #000;
        background: none;

        &:hover {
            color: #fff;
            background: #000;
        }
`

const StyledButtonSecondary = styled(StyledButton)`
    color: ${props => props.outlined ? '#ff01ff' : '#fff'};
    background: ${props => props.outlined ? 'none' : '#ff01ff'};
    border: ${props => props.outlined ? '1px solid #ff01ff' : '1px solid #fff'}; 
`


type Props = {
    title: string;
    outlined?: boolean;
    variant?: 'primary' | 'secondary' | 'ghost';
    onClick?: () => void;
    disabled?: boolean;
}

const getStyledButton = (variant:  'primary' | 'secondary' | 'ghost') => {
    switch (variant) {
        case 'primary':
            return StyledButton;
        case 'secondary':
            return StyledButtonSecondary;
        case 'ghost':
            return StyledButtonGhost;  
        default:
            return StyledButton;     
    }   
}

const Button: React.FC<Props> = ({title, outlined, disabled, onClick}) => {
    return (
        <StyledButton outlined ={!!outlined} onClick={onClick} disabled={disabled}>
            {title}
        </StyledButton>
    );
}

export default Button;
