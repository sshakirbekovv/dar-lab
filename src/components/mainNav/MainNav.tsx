import React, { useEffect, useState } from 'react';
import styles from './MainNav.module.scss';
import {getCategories} from '../../shared/api';
import {Category} from '../../shared/type';
import {Link} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { selectCategories } from '../../shared/redux/categories/categories.selectors';
import { fetchCategories, setCategories } from '../../shared/redux/categories/categories.actions';


const MainNav: React.FC = () => {

    const dispatch = useDispatch();
    // const [items, setItems] = useState<Category[]>([]);

    
    const categories = useSelector(selectCategories);
    console.log('CategoriesL',categories)

    useEffect(() => { 
    //  const fetchData = async () => {
    //   const res = await getCategories();
    //   dispatch(setCategories(res.data));
    //  }
    //  fetchData ();
     dispatch(fetchCategories());
    }, [])

    return (
        <div className={styles.MainNav}>
                <nav className={styles.HeaderNav}>
                    <ul>
                        <li className={styles.HeaderNav__link}><Link to='/articles'>Все статьи</Link></li>
                            {
                             categories && categories.map(categories => (<li key={categories.id} className={styles.HeaderNav__link}><Link to={`/articles/${categories.id}`}>{categories.title}</Link></li>))
                            }
                        <li className={styles.HeaderNav__link}><Link to='/counter'>Счетчик</Link></li>
                    </ul>
                </nav>
        </div>
    );
}

export default MainNav;