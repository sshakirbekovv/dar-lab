import React, { useContext} from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { resetProfile } from '../../shared/redux/auth/auth.actions';
import Button from '../button/Button';
import styles from './Profile.module.scss';

type Props = {
    username: string;
    avatar: string;
}

const Profile: React.FC<Props> = ({username, avatar}) => {
    const history = useHistory(); 
    const dispatch = useDispatch();


    const Logout = () => {
            dispatch(resetProfile());
            localStorage.removeItem('authToken');
            history.replace('/');
    }
    return (
        <div className={styles.header_profile}>
            <img className={styles.profile_img} src={avatar} /> 
            <p>{username}</p>
            <Button title={'Log out'} onClick={Logout}/>
        </div>
    )
}

export default Profile;
