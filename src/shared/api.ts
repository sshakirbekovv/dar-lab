import axios from "axios"
import { Article, Category } from "./type";


const apiBase = 'http://localhost:3333';
export const getCategories = () => {
    return axios.get<Category[]>(`https://media-api.dar-dev.zone/api/categories`);
}

export const getArticle = (id: string) => {
    return axios.get<Article>(`https://media-api.dar-dev.zone/api/articles/${id}`);
}

export const getArticles = (categoryId?: string) => {
    const params: any = {
        sort: 'id:DESC',
        limit: 10
    }

    if (categoryId) {
        params['category'] = categoryId;
    }

    return axios.get<Article[]>(`https://media-api.dar-dev.zone/api/articles`, {
        params,
    })

};

export const login = (username: string, password: string) => {
    return axios.post(`${apiBase}/auth/login`, {
        username,
        password
    })
}


export const getProfile = () => {
    const token = localStorage.getItem('authToken');
    return axios.get(`${apiBase}/auth/profile`, {
        headers: {
            'Authorization': token
        }
    });
}
