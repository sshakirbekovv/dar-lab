import { Article } from "../../type";
import { ArticlesActionTypes } from "./articles.types";

export interface ArticlesAction<T>{
    type: ArticlesActionTypes,
    payload?: T | string
}

export const setArticles = (articles: Article[]): ArticlesAction<Article[]> => {
   return {
       type: ArticlesActionTypes.SET_ARTICLES,
       payload: articles
   }
}

export const fetchArticles = (params: { categoryId: string }): ArticlesAction<any> => {
    return {
        type: ArticlesActionTypes.FETCH_ARTICLES,
        payload: params,
    }
 }

export const fetchArticlesSuccess = (articles: Article[]): ArticlesAction<Article[]> => {
    return {
        type: ArticlesActionTypes.FETCH_ARTICLES_SUCCESS,
        payload: articles
    }
 }

export const fetchArticlesError = (error: any): ArticlesAction<any> => {
    return {
        type: ArticlesActionTypes.FETCH_ARTICLES_ERROR,
        payload: error
    }
 }

 export const fetchArticle = (params: { articleId: string } ): ArticlesAction<any> => {
    return {
      type: ArticlesActionTypes.FETCH_ARTICLE,
      payload: params,
    }
  }
  
  export const fetchArticleSuccess = (article: Article): ArticlesAction<Article> => {
    return {
      type: ArticlesActionTypes.FETCH_ARTICLE_SUCCESS,
      payload: article,
    }
  }
  
  export const fetchArticleError = (error: string): ArticlesAction<any> => {
    return {
      type: ArticlesActionTypes.FETCH_ARTICLE_ERROR,
      payload: error,
    }
  }
  