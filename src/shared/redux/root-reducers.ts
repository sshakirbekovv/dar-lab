import { categoriesReducer } from './categories/categories.reducer';
import { combineReducers } from 'redux';
import { articlesReducer } from './articles/articles.reducer';
import { authReducer } from './auth/auth.reducer';


const rootReducers = combineReducers({
    category: categoriesReducer,
    article: articlesReducer,
    auth: authReducer
});

export type RootState = ReturnType<typeof rootReducers>;

export default rootReducers;
