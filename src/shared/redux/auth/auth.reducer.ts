import { AuthAction, AuthActionTypes } from './auth.types';
import { Profile } from './../../type';

export interface AuthState {
    profile: Profile | null,
    loginError: string;
    loginLoading: boolean;
    profileError: string;
}

const defaultState = {
    profile: null,
    loginError: '',
    loginLoading: false,
    profileError: ''
}

export const authReducer = (state: AuthState = defaultState, action: AuthAction): AuthState => {
    switch(action.type) {
        case AuthActionTypes.LOGIN:
            return {
                ...state,
                loginLoading: true,
                loginError: '', 
            }
        case AuthActionTypes.LOGIN_SUCCESS:
            return {
                ...state,
                loginError: '',
                loginLoading: false,
            }
        case AuthActionTypes.LOGIN_ERROR:
            return {
                ...state,
                loginLoading: false,
                loginError: action.payload
            }
        case AuthActionTypes.SET_PROFILE:
        case AuthActionTypes.FETCH_PROFILE_SUCCESS:
            return {
                ...state,
                profile: action.payload,
            }
        case AuthActionTypes.FETCH_PROFILE_ERROR:
            return {
                ...state,
                profileError: action.payload,
            }
        case AuthActionTypes.RESET_PROFILE:
            return {
                ...state,
                profile: null,
            }
        default:
            return state;
    }
}
