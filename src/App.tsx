import React, { useEffect, useReducer, useState } from 'react';
import './App.scss';
import { Switch, Route } from 'react-router-dom';
import Header from './components/header/Header';
import MainNav from './components/mainNav/MainNav';
import Counter from './components/counter/Counter';
import ArticlePages from './pages/articles/ArticlePages';
import ArticlePage from './pages/article/ArticlePage';
import { Profile } from './shared/type';
import AppContext, { reducer } from './shared/app.context'
import LoginPage from './pages/login/LoginPage';
import { useDispatch } from 'react-redux';
import { fetchProfile } from './shared/redux/auth/auth.actions';

const App:React.FC = () => {

  // const getLocal = (): Profile | null => {
  //   const user = localStorage.getItem('user');
  //   if (user) {
  //     return JSON.parse(user);
  //   } 
  //     return null;
  // }

  // const [state, dispatch, ] = useReducer(reducer, {
  //   profile: getLocal(),
  // })
  const dispatch = useDispatch();

    useEffect(() => {
      const token = localStorage.getItem('authToken')
      if (token) {
        dispatch(fetchProfile());
      }
    }, [])

  return (
      <div className="App">
        <Header />
        <MainNav />
        <Switch>
          <Route path="/counter" render={() => <Counter/>}/>
          <Route exact path="/articles" component={ArticlePages}/>
          <Route path="/articles/:categoryId" component={ArticlePages}/>
          <Route path="/article/:articleId" component={ArticlePage}/>
          <Route path="/login" component={LoginPage} />
        </Switch>
      </div>
  );
}

export default App;
